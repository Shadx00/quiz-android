package fr.mostermans_flachaire.myapp.bo;

import com.google.gson.annotations.SerializedName;

public class TokenResponse {

        @SerializedName("response_code")
        public final int responseCode;
        @SerializedName("response_message")
        public final String responseMessage;
        public final String token;

    public TokenResponse(int responseCode, String responseMessage, String token) {
        this.responseCode = responseCode;
        this.responseMessage = responseMessage;
        this.token = token;
    }



}
