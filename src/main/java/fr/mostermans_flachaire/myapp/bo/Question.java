package fr.mostermans_flachaire.myapp.bo;

import android.os.Build;
import android.text.Html;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Question {

    public final String category;
    public final String type;
    public final String difficulty;
    public final String question;
    @SerializedName("correct_answer")
    public final String correctAnswer;
    @SerializedName("incorrect_answers")
    public final List<String> incorrectAnswers;


    public Question(String category, String type, String difficulty, String question, String correctAnswer, List<String> incorrectAnswers) {
        this.category = category;
        this.type = type;
        this.difficulty = difficulty;
        this.question = question;
        this.correctAnswer = correctAnswer;
        this.incorrectAnswers = incorrectAnswers;
    }


    public List<String> getAnswers(){
        final List<String> answers = new ArrayList<>();
        for (final String value:incorrectAnswers) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                answers.add(Html.fromHtml(value, Html.FROM_HTML_MODE_COMPACT).toString());
            } else {
                answers.add(Html.fromHtml(value).toString());
            }

        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            answers.add(Html.fromHtml(correctAnswer, Html.FROM_HTML_MODE_COMPACT).toString());
        } else {
            answers.add(Html.fromHtml(correctAnswer).toString());
        }
        Collections.shuffle(answers);
        return answers;

    }

    public String getCorrectAnswer() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return Html.fromHtml(correctAnswer, Html.FROM_HTML_MODE_COMPACT).toString();
        } else {
            return Html.fromHtml(correctAnswer).toString();
        }
    }
}
