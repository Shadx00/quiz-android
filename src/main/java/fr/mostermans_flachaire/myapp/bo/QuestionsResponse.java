package fr.mostermans_flachaire.myapp.bo;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class QuestionsResponse {

        @SerializedName("response_code")
        public final int responseCode;
        public final List <Question> results;

    public QuestionsResponse(int responseCode, List<Question> results) {
        this.responseCode = responseCode;
        this.results = results;

    }



}
