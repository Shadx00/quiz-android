package fr.mostermans_flachaire.myapp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;
import androidx.preference.PreferenceManager;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Random;

import fr.mostermans_flachaire.myapp.bo.Question;
import fr.mostermans_flachaire.myapp.services.QuestionServices;


public class MainActivity<score2> extends Activity implements View.OnClickListener {



    Button button1, button2, button3, button4;
    TextView questionView;
    TextView scoreView;
    TextView categoryView;
    TextView difficultyView;
    TextView questionLeftView;
    ProgressBar loader;

    Random random;



    private int  score = 0;
    private List<Question> questions;
    private int index = 0;
    private String correctAnswer;


    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        button1 = (Button) findViewById(R.id.btn_one);
        button1.setOnClickListener(this);
        button2 = (Button) findViewById(R.id.btn_two);
        button2.setOnClickListener(this);
        button3 = (Button) findViewById(R.id.btn_three);
        button3.setOnClickListener(this);
        button4 = (Button) findViewById(R.id.btn_four);
        button4.setOnClickListener(this);

        questionView = (TextView) findViewById(R.id.tv_question);
        scoreView = (TextView) findViewById(R.id.score);
        categoryView = (TextView) findViewById(R.id.category);
        difficultyView = (TextView) findViewById(R.id.difficulty);
        questionLeftView = (TextView) findViewById((R.id.questionleft));
        loader = (ProgressBar) findViewById((R.id.loader));

        getQuestions();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_one:
            case R.id.btn_two:
            case R.id.btn_three:
            case R.id.btn_four:
                final boolean isCorrect = ((Button) v).getText().equals(correctAnswer);
                if (isCorrect) {
                    score = score + 1;
                } else {
                    ((Button) v).setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.red));
                }

                if (button1.getText().equals(correctAnswer)) {
                    button1.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.green));
                } else if (button2.getText().equals(correctAnswer)) {
                    button2.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.green));
                } else if (button3.getText().equals(correctAnswer)) {
                    button3.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.green));
                } else if (button4.getText().equals(correctAnswer)) {
                    button4.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.green));
                }


                Toast.makeText(MainActivity.this, isCorrect ? "You are correct" : "You are incorrect", Toast.LENGTH_SHORT).show();
                button1.setEnabled(false);
                button2.setEnabled(false);
                button3.setEnabled(false);
                button4.setEnabled(false);

                new Thread(() -> {
                    try {
                        Thread.sleep(1200);
                        runOnUiThread(this::nextQuestion);

                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }).start();

                break;

        }
    }

  /*  private String getToken() {
        QuestionServices.getInstance().api.getToken().execute().body().token;
    }*/

    private void getQuestions() {
        loader.setVisibility(View.VISIBLE);
        button1.setVisibility(View.GONE);
        button2.setVisibility(View.GONE);
        button2.setVisibility(View.GONE);
        button3.setVisibility(View.GONE);
        button4.setVisibility(View.GONE);

        final String count = PreferenceManager.getDefaultSharedPreferences(this).getString("count", null);
        int count2;
        try {
            count2 = Integer.parseInt(count);
        } catch (NumberFormatException e) {
            count2 = 10;
        }

        final int count3 = count2;

        final String category = PreferenceManager.getDefaultSharedPreferences(this).getString("category", null);
        final String difficulty = PreferenceManager.getDefaultSharedPreferences(this).getString("difficulty", null);


        new Thread(() -> {
            try {
                questions = QuestionServices.getInstance().api.getQuestions(count3, null, category, difficulty).execute().body().results;
                if (questions != null) {
                    runOnUiThread(this::nextQuestion);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }).start();

    }



    private void nextQuestion() {


        if (index >= questions.size()) {


            endGame();
            return;
        }
        button1.setEnabled(true);
        button2.setEnabled(true);
        button3.setEnabled(true);
        button4.setEnabled(true);

        loader.setVisibility(View.GONE);

        scoreView.setText("Score: " + score);
        questionLeftView.setText("Questions left: " + (questions.size() - index - 1));

        final Question question = questions.get(index);
        final List<String> answers = question.getAnswers();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            questionView.setText(Html.fromHtml(question.question, Html.FROM_HTML_MODE_COMPACT));
        } else {
            questionView.setText(Html.fromHtml(question.question));
        }
        difficultyView.setText(question.difficulty);
        categoryView.setText(question.category);


        button1.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.blue));
        button1.setText(answers.get(0));
        button1.setVisibility(View.VISIBLE);
        button2.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.blue));
        button2.setText(answers.get(1));
        button2.setVisibility(View.VISIBLE);
        button3.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.blue));
        if (answers.size() > 2) {
            button3.setText(answers.get(2));
            button3.setVisibility(View.VISIBLE);
        } else {
            button3.setVisibility(View.GONE);
        }
        button4.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.blue));
        if (answers.size() > 3) {
            button4.setText(answers.get(3));
            button4.setVisibility(View.VISIBLE);
        } else {
            button4.setVisibility(View.GONE);
        }

        correctAnswer = question.getCorrectAnswer();
        index++;


    }


    private void endGame(){


        final SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        final String pseudoText = sharedPreferences.getString("pseudo",null);
        final int oldScore = sharedPreferences.getInt("score_"+ pseudoText,0);

        if(oldScore < score){
            sharedPreferences.edit().putInt("score_"+ pseudoText,score).apply();
        }

        final Intent intent = new Intent(getApplicationContext(),EndActivity.class);
        intent.putExtra("score" ,score);
        startActivity(intent);
        finish();
    }




}