package fr.mostermans_flachaire.myapp;


import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.PreferenceManager;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NavigableMap;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

public class HomeActivity extends AppCompatActivity implements View.OnClickListener {

    Button button, button2;
    TextView pseudo1View,pseudo2View,pseudo3View;
    TextView score1View,score2View,score3View;
    List<TextView> scoreViews = new ArrayList<>();
    List<TextView> pseudoViews = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);


        button = (Button) findViewById(R.id.button);
        button.setOnClickListener(this);
        button2 = (Button) findViewById(R.id.button2);
        button2.setOnClickListener(this);

        pseudo1View = (TextView) findViewById(R.id.pseudo1);
        pseudo2View = (TextView) findViewById(R.id.pseudo2);
        pseudo3View = (TextView) findViewById(R.id.pseudo3);

        score1View = (TextView) findViewById(R.id.score1);
        score2View = (TextView) findViewById(R.id.score2);
        score3View = (TextView) findViewById(R.id.score3);

        scoreViews.add(score1View);
        scoreViews.add(score2View);
        scoreViews.add(score3View);

        pseudoViews.add(pseudo1View);
        pseudoViews.add(pseudo2View);
        pseudoViews.add(pseudo3View);


    }

    @Override
    protected void onResume() {
        super.onResume();

        final SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        final TreeMap<String, Object> map = new TreeMap<>(sharedPreferences.getAll());
        final SortedMap<String, Object> subMap = map.subMap("score_", "score_" + Character.MAX_VALUE);
        List<Map.Entry<String, Object>> entries = new ArrayList<>(subMap.entrySet());
        Collections.sort(entries, (o1, o2) -> -((Integer) o1.getValue()).compareTo((Integer) o2.getValue()));

        entries = entries.subList(0, Math.min(entries.size(),3));

        for (int i = 0; i < entries.size(); i++) {
            final String pseudo = entries.get(i).getKey().replace("score_", "");
            final int score = (int) entries.get(i).getValue();
            pseudoViews.get(i).setText(pseudo);
            scoreViews.get(i).setText(String.valueOf(score));
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button:
                startActivity(new Intent(getApplicationContext(), MainActivity.class));
                break;

            case R.id.button2:
                startActivity(new Intent(getApplicationContext(), SettingsActivity.class));
                break;
        }
    }
}