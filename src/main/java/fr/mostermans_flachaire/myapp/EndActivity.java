package fr.mostermans_flachaire.myapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.PreferenceManager;

public class EndActivity extends AppCompatActivity implements View.OnClickListener {

    TextView pseudoView;
    TextView scoreView;
    Button btnRestart;
    int Score;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.end_layout);


        pseudoView = (TextView) findViewById(R.id.pseudo);
        scoreView = (TextView) findViewById(R.id.score);

        btnRestart = (Button) findViewById(R.id.btn_restart);
        btnRestart.setOnClickListener(this);

        final SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        final String pseudo = sharedPreferences.getString("pseudo",null);
        final int score = getIntent().getIntExtra("score",0);


        scoreView.setText("Score : " + score);
        pseudoView.setText("Name : " + pseudo );


    }


    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_restart:
                finish();
                break;
        }
    }

}
