package fr.mostermans_flachaire.myapp.services;

import fr.mostermans_flachaire.myapp.bo.QuestionsResponse;
import fr.mostermans_flachaire.myapp.bo.TokenResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface QuestionAPI {
    @GET("/api_token.php?command=request")
    Call<TokenResponse> getToken();

    @GET("/api.php")
    Call<QuestionsResponse> getQuestions(@Query("amount") int amount, @Query("token") String token, @Query("category") String category, @Query("difficulty") String difficulty);

}


