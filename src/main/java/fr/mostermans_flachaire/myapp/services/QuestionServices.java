package fr.mostermans_flachaire.myapp.services;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class QuestionServices {

    private static QuestionServices instance;
    private final Retrofit retrofit;
    public final QuestionAPI api;

    private QuestionServices() {
        retrofit = new Retrofit.Builder()
                .baseUrl("https://opentdb.com")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        api = retrofit.create(QuestionAPI.class);
    }

    public static QuestionServices getInstance() {
        if (instance == null) {
            instance = new QuestionServices();
        }
        return instance;
    }


}
